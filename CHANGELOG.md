# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v1.1.2](https://framagit.org/yphil/petrolette/compare/v1.1.2...v1.1.2)

### Commits

- Error handling: npm run errors in package.json [`8115ed7`](https://framagit.org/yphil/petrolette/commit/8115ed70ff39ae1874b8e3d2fde7305e3f8f00ee)
- (auto) Changelog tests [`3e7411a`](https://framagit.org/yphil/petrolette/commit/3e7411a0970676f7affc35fc3dc610f0cd202c58)
- Big conflict ahead [`b06e675`](https://framagit.org/yphil/petrolette/commit/b06e675ba520073dce0e0e324d5ec27c09d358a3)

## [v1.1.2](https://framagit.org/yphil/petrolette/compare/v1.0.46...v1.1.2) - 2021-02-02

### Commits

- Fold / unfold icon OK, no anim yet [`c8e36be`](https://framagit.org/yphil/petrolette/commit/c8e36bec1757483a2355251e9fe16c30c53e5311)
- Icons makeover & fontello files cleanup [`4e94502`](https://framagit.org/yphil/petrolette/commit/4e9450258b2dd911021e6aad4840e2ab7c3b3c0f)
- Feed status OK from prefs to refresh [`e37c8a8`](https://framagit.org/yphil/petrolette/commit/e37c8a8385c8261a1403889f49d4c5f79cfaaa89)

## [v1.0.46](https://framagit.org/yphil/petrolette/compare/v1.0.45...v1.0.46) - 2021-01-12

### Commits

- Version 1.0.45 "Iconv woes" - 12/01/2021 00:23:13 [`d6f3c22`](https://framagit.org/yphil/petrolette/commit/d6f3c2275559f188820f16c2612b2445e0519952)
- Version 1.0.45 "Iconv woes" - 12/01/2021 00:23:13 [`a7b0fd6`](https://framagit.org/yphil/petrolette/commit/a7b0fd61b95b08f0b76f5f08a572a4b1b8be453d)

## [v1.0.45](https://framagit.org/yphil/petrolette/compare/v1.0.44...v1.0.45) - 2021-01-10

### Commits

- Version 1.0.44 "Latest node + sudo bower" - 10/01/2021 23:00:25 [`ef27a7c`](https://framagit.org/yphil/petrolette/commit/ef27a7c273f35384b98dcd7398526de12968a5f0)
- Version 1.0.44 "Latest node + sudo bower" - 10/01/2021 23:00:25 [`02e29f5`](https://framagit.org/yphil/petrolette/commit/02e29f5c4f8fd38d240968e1c5f2eda64c7abf34)

## [v1.0.44](https://framagit.org/yphil/petrolette/compare/v1.0.43...v1.0.44) - 2021-01-10

### Commits

- Version 1.0.43 "Latest node" - 10/01/2021 22:56:41 [`9511273`](https://framagit.org/yphil/petrolette/commit/9511273f7ae945da750076b223ef94c2f2248ffc)
- Version 1.0.43 "Latest node" - 10/01/2021 22:56:41 [`06dc91a`](https://framagit.org/yphil/petrolette/commit/06dc91a1ea1568c43afda71540f8048d9854722d)

## [v1.0.43](https://framagit.org/yphil/petrolette/compare/v1.0.42...v1.0.43) - 2021-01-10

### Commits

- Version 1.0.42 "CI: npm i" - 10/01/2021 22:12:20 [`b20b3f5`](https://framagit.org/yphil/petrolette/commit/b20b3f547dc964bfb67873c03d42763f2b16429b)
- Version 1.0.42 "CI: npm i" - 10/01/2021 22:12:20 [`67f2230`](https://framagit.org/yphil/petrolette/commit/67f22302d581632cc712b6c22ae2423f0c8a4cbc)

## [v1.0.42](https://framagit.org/yphil/petrolette/compare/v1.0.41...v1.0.42) - 2021-01-10

### Commits

- Version 1.0.41 "Latest node in CI" - 10/01/2021 22:09:36 [`9757fe0`](https://framagit.org/yphil/petrolette/commit/9757fe0c950d51cfd65c6dde3e7a093f8eb2f64e)
- Version 1.0.41 "Latest node in CI" - 10/01/2021 22:09:36 [`3b5c8a4`](https://framagit.org/yphil/petrolette/commit/3b5c8a4624743398aaa3c94cbb9eab486edae70e)

## [v1.0.41](https://framagit.org/yphil/petrolette/compare/v1.0.40...v1.0.41) - 2021-01-10

### Commits

- Version 1.0.40 "New jobs" - 10/01/2021 22:01:29 [`ab01086`](https://framagit.org/yphil/petrolette/commit/ab01086a32bd02770666c9a6e798502c4c9fdd4a)
- Version 1.0.40 "New jobs" - 10/01/2021 22:01:29 [`3594f1f`](https://framagit.org/yphil/petrolette/commit/3594f1f06b9166c262705e00e8027631da2b13e7)

## [v1.0.40](https://framagit.org/yphil/petrolette/compare/v1.0.39...v1.0.40) - 2021-01-10

### Commits

- Version 1.0.39 "Version bump: latest everything" - 10/01/2021 21:45:57 [`010c04b`](https://framagit.org/yphil/petrolette/commit/010c04bdcc59c7dae5e71757e015a5b310cda0f8)
- Version 1.0.39 "Version bump: latest everything" - 10/01/2021 21:45:57 [`aeb25c4`](https://framagit.org/yphil/petrolette/commit/aeb25c460cf8edd3434f7464cabf1547d26a38de)

## [v1.0.39](https://framagit.org/yphil/petrolette/compare/v1.0.38...v1.0.39) - 2021-01-10

### Commits

- Version 1.0.38 "PM2 Version bump" - 10/01/2021 21:38:06 [`59bce22`](https://framagit.org/yphil/petrolette/commit/59bce226123f388e826cc43678d8f0e7afe28cf6)
- Version 1.0.38 "PM2 Version bump" - 10/01/2021 21:38:06 [`1b8378a`](https://framagit.org/yphil/petrolette/commit/1b8378aadcbe0a19b126394cd4f1a020596795c4)

## [v1.0.38](https://framagit.org/yphil/petrolette/compare/v1.0.37...v1.0.38) - 2021-01-10

### Commits

- Version 1.0.37 "NodeJs Version bump" - 10/01/2021 21:33:48 [`1dd8337`](https://framagit.org/yphil/petrolette/commit/1dd833777dea3c5f0b92c8b980726e2b92cc6275)

## [v1.0.37](https://framagit.org/yphil/petrolette/compare/v1.0.36...v1.0.37) - 2021-01-10

### Commits

- Version 1.0.36 "NodeJs Version bump" - 10/01/2021 21:31:29 [`b11c950`](https://framagit.org/yphil/petrolette/commit/b11c9501ff7ed2adce0eb105ad698dadbbd1d406)
- Version 1.0.36 "NodeJs Version bump" - 10/01/2021 21:31:29 [`ad567f0`](https://framagit.org/yphil/petrolette/commit/ad567f08b98682ff80773a4ea4e4d85c744747ab)

## [v1.0.36](https://framagit.org/yphil/petrolette/compare/v1.0.35...v1.0.36) - 2021-01-08

### Commits

- Version 1.0.35 "Testing version bump CI response" - 08/01/2021 13:58:55 [`eaa337a`](https://framagit.org/yphil/petrolette/commit/eaa337a368d882bc2b3fa02d5a3f363fcdcd9786)
- Version 1.0.35 "Testing version bump CI response" - 08/01/2021 13:58:55 [`0125809`](https://framagit.org/yphil/petrolette/commit/01258093316f622a790cde71dbd4482d847d2a80)

## [v1.0.35](https://framagit.org/yphil/petrolette/compare/v1.0.34...v1.0.35) - 2021-01-08

### Commits

- XKCD summary in the title of the description XML tag (Issue #44) [`c6ef488`](https://framagit.org/yphil/petrolette/commit/c6ef48860aa06b430339b4c65f31d074fd75b930)
- Libs version bump [`eb06e09`](https://framagit.org/yphil/petrolette/commit/eb06e09eb01e360c7142917af46d06ecc2ce894e)
- CSS sugar: Console resize pointer & Gallery button:link and:hover colors [`15e76da`](https://framagit.org/yphil/petrolette/commit/15e76dadc27a82dfed144d3008b5a9e9b7ade521)

## [v1.0.34](https://framagit.org/yphil/petrolette/compare/v1.0.33...v1.0.34) - 2020-08-07

### Commits

- Version 1.0.33 "Corona Circus (beta ends)" - 07/08/2020 15:00:42 [`4a6b82f`](https://framagit.org/yphil/petrolette/commit/4a6b82f47d00f48e6d420a3857576d1a824e37ba)
- Version 1.0.33 "Corona Circus (beta ends)" - 07/08/2020 15:00:42 [`0dc5cc0`](https://framagit.org/yphil/petrolette/commit/0dc5cc0e06fd52cf21282270a9fc261027d6f049)

## [v1.0.33](https://framagit.org/yphil/petrolette/compare/v1.0.32...v1.0.33) - 2020-08-07

### Commits

- Version Corona Circus - 07/08/2020 14:47:03 [`ae6be21`](https://framagit.org/yphil/petrolette/commit/ae6be21e50940370b3e45620f7bda1cb78c79a78)

## [v1.0.32](https://framagit.org/yphil/petrolette/compare/v1.0.30...v1.0.32) - 2020-08-07

### Commits

- Version Corona Circus - 07/08/2020 14:43:35 [`5f76a4a`](https://framagit.org/yphil/petrolette/commit/5f76a4a848004dfd57b7ca977700a32babcc7aae)
- Alternate environment CLI args [`450f735`](https://framagit.org/yphil/petrolette/commit/450f7354278f75351eff3bd92fb221f1cd2c0ee4)
- Description in YTube channel items [`d658ffa`](https://framagit.org/yphil/petrolette/commit/d658ffa8d1c415975eda4ccadcda4960584ecba0)

## [v1.0.30](https://framagit.org/yphil/petrolette/compare/v1.0.29...v1.0.30) - 2019-07-15

### Commits

- Lib update: npm & pm2 [`a76490f`](https://framagit.org/yphil/petrolette/commit/a76490fe1d7af98039d8b4cc2b8e879faa0be6ab)
- Version %s "" - 15/07/2019 10:21:45 [`8649591`](https://framagit.org/yphil/petrolette/commit/86495919bd246218236fbdf2c5ff35a12a56a953)
- Lib update [`a663083`](https://framagit.org/yphil/petrolette/commit/a6630837d250f1e1ef50461428e931d47e616dbb)

## [v1.0.29](https://framagit.org/yphil/petrolette/compare/v1.0.28...v1.0.29) - 2019-01-15

### Commits

- Error handling, translation (iconv) and g-unzipping of the feeds [`9c0a6a3`](https://framagit.org/yphil/petrolette/commit/9c0a6a391a2beb11298355f20336359913a8a934)
- Bug: npm i -s body-parser [`0aaec4b`](https://framagit.org/yphil/petrolette/commit/0aaec4bce07651ffe4a429f1664a363e05eb29b7)
- Error handling: toString [`4c3a115`](https://framagit.org/yphil/petrolette/commit/4c3a11506ed764894d60d64242a26b35eaae03da)

## [v1.0.28](https://framagit.org/yphil/petrolette/compare/v1.0.26...v1.0.28) - 2018-11-30

### Commits

- Version %s "Mon ami le gilet jaune" - 30/11/2018 13:35:53 [`644a9f1`](https://framagit.org/yphil/petrolette/commit/644a9f127feeef4900c7c45ebea70ae2d8c67ba2)
- Lib update + small bugfix (content-type check in index) [`8e27576`](https://framagit.org/yphil/petrolette/commit/8e2757681f1ba574384369393c261bcc9e3b1d56)
- Lib Update: pm2 - 2.10.4 =&gt; 3.0.3 [`33ba5d6`](https://framagit.org/yphil/petrolette/commit/33ba5d61ca1b9ff0c14ac1cdb39c5668008df392)

## [v1.0.26](https://framagit.org/yphil/petrolette/compare/v1.0.25...v1.0.26) - 2018-04-02

### Commits

- Sys: HACK: PM2 force install [`f873979`](https://framagit.org/yphil/petrolette/commit/f873979414aa09aaf982cc898386b0295e549d71)
- Sys: Finally removed fsevents from deps [`b5e77b8`](https://framagit.org/yphil/petrolette/commit/b5e77b890ae8a7e28a37620800a1da5791e85c19)
- Feed: BUG: $selectIcon class(es) misshap [`ecea6ee`](https://framagit.org/yphil/petrolette/commit/ecea6ee697d9f480bb8d3e1a3c149e1998349fd3)

## [v1.0.25](https://framagit.org/yphil/petrolette/compare/v1.0.24...v1.0.25) - 2018-03-26

### Commits

- Sys: Tests: npm update [`43156a6`](https://framagit.org/yphil/petrolette/commit/43156a6ea90035c28da42cf259bb563f0a57fa25)
- UI: Coherent color class scheme [`20b8208`](https://framagit.org/yphil/petrolette/commit/20b82081fd8262151326fb00e15830456c206229)
- Cache: Promise fulfilled :) [`7fc44ed`](https://framagit.org/yphil/petrolette/commit/7fc44ed9ddb5087eb41258468077ff6e465f8e04)

## [v1.0.24](https://framagit.org/yphil/petrolette/compare/v1.0.23...v1.0.24) - 2018-03-19

### Commits

- Cols: newPopulate: Counting sources OK [`b96ff78`](https://framagit.org/yphil/petrolette/commit/b96ff786b5985131d58454f21fce2cd6e6d8fe99)
- Version 1.0.24 "Toot Sweet" - 19/03/2018 07:10:28 [`156860c`](https://framagit.org/yphil/petrolette/commit/156860c126305df4951c9ace7cff5d296637af75)
- merging [`f69f6e7`](https://framagit.org/yphil/petrolette/commit/f69f6e76c9c9aabab7db01df8d7bd10f78b13485)

## [v1.0.23](https://framagit.org/yphil/petrolette/compare/v1.0.22...v1.0.23) - 2018-03-08

### Commits

- Sys: Flat JS [`b9a303d`](https://framagit.org/yphil/petrolette/commit/b9a303d48df65ed0f554a30c2953f84ee00c8435)
- Sys: mv public/lib/js/ public/ [`daae99e`](https://framagit.org/yphil/petrolette/commit/daae99e66c20623d1959ba9e9315960af3903809)
- Sys: Licencing info: All Pétrolette PASSED LibreJS parsing [`29f51b9`](https://framagit.org/yphil/petrolette/commit/29f51b9e7d957be9c566d8f79d4f6dffa05b63a4)

## [v1.0.22](https://framagit.org/yphil/petrolette/compare/v1.0.21...v1.0.22) - 2018-03-06

### Commits

- Sys: Query-string enabled ; Now a call to petrolette.url?source=plop creates a new "plop" source [`ae0cbbb`](https://framagit.org/yphil/petrolette/commit/ae0cbbbd23a4b322ba64448d64c78aae14bff390)
- Version 1.0.22 "New Codename" - 06/03/2018 18:11:46 [`08ba5b2`](https://framagit.org/yphil/petrolette/commit/08ba5b2470978869c6095abd560727d480ecc459)
- Sys: Cleaning up before new "q-string" branch [`5ae74cf`](https://framagit.org/yphil/petrolette/commit/5ae74cf07e4f227d19dd3da3cc2707dbc226b5b4)

## [v1.0.21](https://framagit.org/yphil/petrolette/compare/v1.0.20...v1.0.21) - 2018-03-06

### Commits

- Version 1.0.21 - 06/03/2018 16:22:38 [`9de1d8d`](https://framagit.org/yphil/petrolette/commit/9de1d8dbb86ebe959ab8df666824b3bcc52691b8)
- Version %s - 06/03/2018 16:22:38 [`d296158`](https://framagit.org/yphil/petrolette/commit/d296158e50d14212bf05451c4ad2e0a125c5a913)

## [v1.0.20](https://framagit.org/yphil/petrolette/compare/v1.0.19...v1.0.20) - 2018-03-06

### Commits

- Sys: Console messages handling (type & translations) [`86e6433`](https://framagit.org/yphil/petrolette/commit/86e6433b537b70b40d09f3e2f57f84ea9dc19142)
- All: &lt;base target="_blank"&gt; links open in a new tab by default [`78a0360`](https://framagit.org/yphil/petrolette/commit/78a0360ac17f40960b95e542a0db1d925c9468f5)
- Version 1.0.20 - 06/03/2018 16:20:21 [`453d782`](https://framagit.org/yphil/petrolette/commit/453d78202f71827d70c03ec65bebe911acf2b293)

## [v1.0.19](https://framagit.org/yphil/petrolette/compare/v1.0.18...v1.0.19) - 2018-03-06

### Commits

- Feed: Icons (mobile) margins [`3dc8770`](https://framagit.org/yphil/petrolette/commit/3dc8770dca39772509479ff6cad16b11339c1bae)
- Feed: Icons (mobile) margins - 1.0.19 [`252f712`](https://framagit.org/yphil/petrolette/commit/252f712bd105cec8d8f610e90fc08b2b88cf519d)

## [v1.0.18](https://framagit.org/yphil/petrolette/compare/v1.0.17...v1.0.18) - 2018-03-06

### Commits

- Sys: No more images (icons) in UI - 1.0.18 [`be137f2`](https://framagit.org/yphil/petrolette/commit/be137f2e5897018e814fe2b0441dd992a3b4d5c8)
- Sys: No more images (icons) in UI [`be58115`](https://framagit.org/yphil/petrolette/commit/be58115174d4c677dea698ff75d1ed6cc355434e)

## [v1.0.17](https://framagit.org/yphil/petrolette/compare/v1.0.16...v1.0.17) - 2018-03-06

### Commits

- Feed: icon-rss-squared is now an icon [`0988198`](https://framagit.org/yphil/petrolette/commit/0988198ba843ad2c57d32ace488274bda7d1b975)
- Feed: $feedToggle is now an icon [`dd30a4e`](https://framagit.org/yphil/petrolette/commit/dd30a4e33983d3272231fa9c1fa93a1c16c5f514)
- Sys: No more images (icons) in UI - 1.0.17 [`4039489`](https://framagit.org/yphil/petrolette/commit/403948949c91ef69dda8c8c7be9a8854a3c696c3)

## [v1.0.16](https://framagit.org/yphil/petrolette/compare/v1.0.15...v1.0.16) - 2018-03-05

### Commits

- Feed: Broken favicons investigation WIP - 1.0.16 [`14de5e7`](https://framagit.org/yphil/petrolette/commit/14de5e7497c604f343fe4c599841992cde8a54a2)

## [v1.0.15](https://framagit.org/yphil/petrolette/compare/v1.0.14...v1.0.15) - 2018-03-05

### Commits

- Menu: i.icon-petrolette.writing [`f469cef`](https://framagit.org/yphil/petrolette/commit/f469cefcde9bf7b5b92223c8a5e36e569f81d78e)
- Menu: i.icon-petrolette.writing - 1.0.15 [`e658c24`](https://framagit.org/yphil/petrolette/commit/e658c243a9a4f9cfe69074978867502953d8bf6c)

## [v1.0.14](https://framagit.org/yphil/petrolette/compare/v1.0.13...v1.0.14) - 2018-03-05

### Commits

- Feed: a:pseudo classes precedence (and the !important madness) [`287b40a`](https://framagit.org/yphil/petrolette/commit/287b40adb3d61f672e39f43c36195e027bf3b6df)
- Feed: a:pseudo classes precedence (and the !important madness) - 1.0.14 [`2774453`](https://framagit.org/yphil/petrolette/commit/277445363b66c373735bfbbb41a944b3cfb10482)

## [v1.0.13](https://framagit.org/yphil/petrolette/compare/v1.0.12...v1.0.13) - 2018-03-05

### Commits

- Sys: Menu in module refactoring: Classes & IDs OK [`e2e3fe0`](https://framagit.org/yphil/petrolette/commit/e2e3fe025677c8449489b27799374bb4cbb22e8b)
- Sys: Menu in module refactoring: Classes & IDs WIP [`a438661`](https://framagit.org/yphil/petrolette/commit/a4386610476bc7590a9a40e79bc13fbefec45c7e)
- Spinning elements are always --ui-color-sec [`f0abf69`](https://framagit.org/yphil/petrolette/commit/f0abf69b4269260714a8cd52092f6d8b14c360ab)

## [v1.0.12](https://framagit.org/yphil/petrolette/compare/v1.0.11...v1.0.12) - 2018-03-03

### Commits

- Sys: Themes Refactoring: Files cleanup [`8f64cac`](https://framagit.org/yphil/petrolette/commit/8f64cac303633175b5d34ab8b558cd130781d062)
- Sys: Themes Refactoring: Files cleanup - 1.0.12 [`790082a`](https://framagit.org/yphil/petrolette/commit/790082a3130bed53f94b95e8d7e7c5a207ae9666)

## [v1.0.11](https://framagit.org/yphil/petrolette/compare/v1.0.10...v1.0.11) - 2018-03-03

### Commits

- Sys: Themes Refactoring: One "base" dir [`dab9225`](https://framagit.org/yphil/petrolette/commit/dab9225f0a7eeefc7eb27cc7273a398a3c9de4c9)
- Sys: Themes Refactoring: One "base" dir - 1.0.11 [`af768ca`](https://framagit.org/yphil/petrolette/commit/af768ca6b31f3253dc1f339f622b91ebb5288bda)

## [v1.0.10](https://framagit.org/yphil/petrolette/compare/v1.0.9...v1.0.10) - 2018-03-03

### Commits

- Sys: Huge refactoring, everything is now in the PLT Module namespace [`c43b37d`](https://framagit.org/yphil/petrolette/commit/c43b37d849a132bcfda856838a232be99a95b17f)
- Sys: Huge refactoring, everything is now in the PLT Module namespace - 1.0.10 [`cb095fd`](https://framagit.org/yphil/petrolette/commit/cb095fdfa30aa8b48d105665e31ecc6228a3f307)

## [v1.0.9](https://framagit.org/yphil/petrolette/compare/v1.0.8...v1.0.9) - 2018-03-03

### Commits

- Sys: Huge refactoring, everything is now in the PLT Module namespace [`1d13160`](https://framagit.org/yphil/petrolette/commit/1d13160dabf1505df69b5cdc5f3b91da73b9a54c)
- Feed: (small) Ajax calls refactoring [`a0ffed7`](https://framagit.org/yphil/petrolette/commit/a0ffed73a31e22e8fd9a1e5654c94f26aea07d72)
- Sys: Huge refactoring, everything is now in the PLT Module namespace - 1.0.9 [`5172c8f`](https://framagit.org/yphil/petrolette/commit/5172c8f17244c3f25a9db12bc6beba9d6c3efbfa)

## [v1.0.8](https://framagit.org/yphil/petrolette/compare/v1.0.7...v1.0.8) - 2018-03-02

### Commits

- Feed: Media icons in text (all) feeds [`f0d355f`](https://framagit.org/yphil/petrolette/commit/f0d355f06c0c8bd8b3707649ce300a71451bb3a8)
- Feed: Media icons in text (all) feeds - 1.0.8 [`15d88a3`](https://framagit.org/yphil/petrolette/commit/15d88a3f1c75756891e3ec5b46c05a8cd37bb2fc)

## [v1.0.7](https://framagit.org/yphil/petrolette/compare/v1.0.6...v1.0.7) - 2018-03-01

### Commits

- Feed: Comments section link [`2fadf05`](https://framagit.org/yphil/petrolette/commit/2fadf05ecf7bb3e1137d37c7b3103f678325950f)
- Feed: Title or URL (#Fix) [`d5bc587`](https://framagit.org/yphil/petrolette/commit/d5bc58753d89a1f1aa71fa9da446ab846628a396)
- Feed: Comments section link - 1.0.7 [`6b0c8d9`](https://framagit.org/yphil/petrolette/commit/6b0c8d933b245d981d1947c90f9258e2f5459b06)

## [v1.0.6](https://framagit.org/yphil/petrolette/compare/v1.0.5...v1.0.6) - 2018-03-01

### Commits

- Feed: Prevent long lines (URLs) from breaking li.feedItem [`7fa2f32`](https://framagit.org/yphil/petrolette/commit/7fa2f3226958ccde792cf5c5da30865f8e82b055)
- Feed: Prevent long lines (URLs) from breaking li.feedItem - 1.0.6 [`527631f`](https://framagit.org/yphil/petrolette/commit/527631f3609f4cbf927a12a89ce9cca00c6616be)

## [v1.0.5](https://framagit.org/yphil/petrolette/compare/v1.0.4...v1.0.5) - 2018-03-01

### Commits

- Feed: Prevent long lines (URLs) from breaking li.feedItem [`4acc648`](https://framagit.org/yphil/petrolette/commit/4acc648b419e82b551345578e2289d6fd5634b15)
- Help: Translations and stuff [`2fc9017`](https://framagit.org/yphil/petrolette/commit/2fc9017ae8180572b9c6dc1ffe919c07c027447f)
- Feed: Prevent long lines (URLs) from breaking li.feedItem - 1.0.5 [`074f2f1`](https://framagit.org/yphil/petrolette/commit/074f2f1421f05ef45ab63455e9ed0d753b9718f8)

## [v1.0.4](https://framagit.org/yphil/petrolette/compare/v1.0.3...v1.0.4) - 2018-03-01

### Commits

- Front: Social media METAs: description - 1.0.4 [`bc9a8c4`](https://framagit.org/yphil/petrolette/commit/bc9a8c447e4deb43958a88e960bcebbcd12524e4)
- Front: Social media METAs: description [`69a63d3`](https://framagit.org/yphil/petrolette/commit/69a63d349959bd233366ac9f18e410e7bb1195e3)

## [v1.0.3](https://framagit.org/yphil/petrolette/compare/v1.0.2...v1.0.3) - 2018-03-01

### Commits

- Front: Social media METAs [`b0f39d6`](https://framagit.org/yphil/petrolette/commit/b0f39d685d11987b9ca29b263deb8e64269211b2)
- Front: Social media METAs - 1.0.3 [`ad22a4b`](https://framagit.org/yphil/petrolette/commit/ad22a4b31320a7a4b60d7d7fd97f22b706a90a0d)

## [v1.0.2](https://framagit.org/yphil/petrolette/compare/v1.0.1...v1.0.2) - 2018-03-01

### Commits

- Sys: npm auto version bump testing [`2910d12`](https://framagit.org/yphil/petrolette/commit/2910d12890c5c8dd9da014bd66717846120eef76)

## v1.0.1 - 2018-03-01

### Merged

- cache feature for favicon [`#7`](https://framagit.org/yphil/petrolette/pull/7)
- correction for image detection and better use of cache [`#4`](https://framagit.org/yphil/petrolette/pull/4)
- link selection: considers both the type and relation of link to avoid po... [`#3`](https://framagit.org/yphil/petrolette/pull/3)

### Commits

- JQuery CDNs [`a742466`](https://framagit.org/yphil/petrolette/commit/a7424667c966dd59cf6d624c4889a20ff1697e2f)
- SYS: Favicons [`2c84063`](https://framagit.org/yphil/petrolette/commit/2c840636cc5098566be4af69c3277a49562dabdd)
- SYNC: Menu integration [`cdd48ae`](https://framagit.org/yphil/petrolette/commit/cdd48aefdc1c3d2509adb2853e07ed5af2d1bad3)
